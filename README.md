# Cliente SOAP CGR

Cliente SOAP para consumir WS de notificación de CGR.

## Requerimientos
 - Java 1.8
 - Maven
 - Cadena de certificados de seguridad 

## Configuraciones
El archivo soap-cgr.properties incluye los datos de configuración del WSDL de CGR. 
Es necesario modificarlo para configurar el ambiente de producción.
