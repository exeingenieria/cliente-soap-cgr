package cl.exe.interoperabilidad.cgr.sistradoc.demo;

import cl.exe.interoperabilidad.cgr.sistradoc.GetEstadoDocRequest;
import cl.exe.interoperabilidad.cgr.sistradoc.GetEstadoDocResponse;
import cl.exe.interoperabilidad.cgr.sistradoc.TomadorDeRazon;

/**
 * Hello world!
 *
 */
public class MainGetEstadoDoc 
{
    public static void main( String[] args )
    {
    	try {
	    	TomadorDeRazon tomador = new TomadorDeRazon();
	    	
	    	GetEstadoDocRequest req = new GetEstadoDocRequest();
	    	req.setAgno("2018");
	    	req.setCodRegion("13");
	    	req.setFolioDoc("349");
	    	req.setServicio("352");
	    	req.setTipoDocCod("02");
	    	
	    	GetEstadoDocResponse res = tomador.getEstadoDoc(req);
	    	
	    	if (res.getGetEstadoDocReturn() != null) {
	    		System.out.println("Exito: " + res.getGetEstadoDocReturn());
	    	} else {
	    		System.out.println("Error: " + res.getError());
	    	}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
