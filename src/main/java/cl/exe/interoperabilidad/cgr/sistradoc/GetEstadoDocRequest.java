
package cl.exe.interoperabilidad.cgr.sistradoc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="servicio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="preFijo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="folioDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="postFijo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="agno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codRegion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoDocCod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "servicio",
    "preFijo",
    "folioDoc",
    "postFijo",
    "agno",
    "codRegion",
    "tipoDocCod"
})
@XmlRootElement(name = "getEstadoDocRequest")
public class GetEstadoDocRequest {

    @XmlElement(required = true)
    protected String servicio;
    @XmlElement(required = true)
    protected String preFijo;
    @XmlElement(required = true)
    protected String folioDoc;
    @XmlElement(required = true)
    protected String postFijo;
    @XmlElement(required = true)
    protected String agno;
    @XmlElement(required = true)
    protected String codRegion;
    @XmlElement(required = true)
    protected String tipoDocCod;

    /**
     * Gets the value of the servicio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicio() {
        return servicio;
    }

    /**
     * Sets the value of the servicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicio(String value) {
        this.servicio = value;
    }

    /**
     * Gets the value of the preFijo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreFijo() {
        return preFijo;
    }

    /**
     * Sets the value of the preFijo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreFijo(String value) {
        this.preFijo = value;
    }

    /**
     * Gets the value of the folioDoc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioDoc() {
        return folioDoc;
    }

    /**
     * Sets the value of the folioDoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioDoc(String value) {
        this.folioDoc = value;
    }

    /**
     * Gets the value of the postFijo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostFijo() {
        return postFijo;
    }

    /**
     * Sets the value of the postFijo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostFijo(String value) {
        this.postFijo = value;
    }

    /**
     * Gets the value of the agno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgno() {
        return agno;
    }

    /**
     * Sets the value of the agno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgno(String value) {
        this.agno = value;
    }

    /**
     * Gets the value of the codRegion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodRegion() {
        return codRegion;
    }

    /**
     * Sets the value of the codRegion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodRegion(String value) {
        this.codRegion = value;
    }

    /**
     * Gets the value of the tipoDocCod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocCod() {
        return tipoDocCod;
    }

    /**
     * Sets the value of the tipoDocCod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocCod(String value) {
        this.tipoDocCod = value;
    }

}
