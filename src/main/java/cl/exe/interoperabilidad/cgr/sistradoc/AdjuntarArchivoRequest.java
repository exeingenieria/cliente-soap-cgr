
package cl.exe.interoperabilidad.cgr.sistradoc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="servicio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codRegion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoDocCod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="preFijo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="folioDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="postFijo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="anno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoAdj" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="archivoUTF8B64" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nombreArchivo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MimeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="enviadoPor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "servicio",
    "codRegion",
    "tipoDocCod",
    "preFijo",
    "folioDoc",
    "postFijo",
    "anno",
    "tipoAdj",
    "descripcion",
    "archivoUTF8B64",
    "nombreArchivo",
    "mimeType",
    "enviadoPor"
})
@XmlRootElement(name = "adjuntarArchivoRequest")
public class AdjuntarArchivoRequest {

    @XmlElement(required = true)
    protected String servicio;
    @XmlElement(required = true)
    protected String codRegion;
    @XmlElement(required = true)
    protected String tipoDocCod;
    @XmlElement(required = true)
    protected String preFijo;
    @XmlElement(required = true)
    protected String folioDoc;
    @XmlElement(required = true)
    protected String postFijo;
    @XmlElement(required = true)
    protected String anno;
    @XmlElement(required = true)
    protected String tipoAdj;
    @XmlElement(required = true)
    protected String descripcion;
    @XmlElement(required = true)
    protected String archivoUTF8B64;
    @XmlElement(required = true)
    protected String nombreArchivo;
    @XmlElement(name = "MimeType", required = true)
    protected String mimeType;
    @XmlElement(required = true)
    protected String enviadoPor;

    /**
     * Gets the value of the servicio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicio() {
        return servicio;
    }

    /**
     * Sets the value of the servicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicio(String value) {
        this.servicio = value;
    }

    /**
     * Gets the value of the codRegion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodRegion() {
        return codRegion;
    }

    /**
     * Sets the value of the codRegion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodRegion(String value) {
        this.codRegion = value;
    }

    /**
     * Gets the value of the tipoDocCod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocCod() {
        return tipoDocCod;
    }

    /**
     * Sets the value of the tipoDocCod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocCod(String value) {
        this.tipoDocCod = value;
    }

    /**
     * Gets the value of the preFijo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreFijo() {
        return preFijo;
    }

    /**
     * Sets the value of the preFijo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreFijo(String value) {
        this.preFijo = value;
    }

    /**
     * Gets the value of the folioDoc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioDoc() {
        return folioDoc;
    }

    /**
     * Sets the value of the folioDoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioDoc(String value) {
        this.folioDoc = value;
    }

    /**
     * Gets the value of the postFijo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostFijo() {
        return postFijo;
    }

    /**
     * Sets the value of the postFijo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostFijo(String value) {
        this.postFijo = value;
    }

    /**
     * Gets the value of the anno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnno() {
        return anno;
    }

    /**
     * Sets the value of the anno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnno(String value) {
        this.anno = value;
    }

    /**
     * Gets the value of the tipoAdj property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoAdj() {
        return tipoAdj;
    }

    /**
     * Sets the value of the tipoAdj property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoAdj(String value) {
        this.tipoAdj = value;
    }

    /**
     * Gets the value of the descripcion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Sets the value of the descripcion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    /**
     * Gets the value of the archivoUTF8B64 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArchivoUTF8B64() {
        return archivoUTF8B64;
    }

    /**
     * Sets the value of the archivoUTF8B64 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArchivoUTF8B64(String value) {
        this.archivoUTF8B64 = value;
    }

    /**
     * Gets the value of the nombreArchivo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreArchivo() {
        return nombreArchivo;
    }

    /**
     * Sets the value of the nombreArchivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreArchivo(String value) {
        this.nombreArchivo = value;
    }

    /**
     * Gets the value of the mimeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMimeType() {
        return mimeType;
    }

    /**
     * Sets the value of the mimeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMimeType(String value) {
        this.mimeType = value;
    }

    /**
     * Gets the value of the enviadoPor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnviadoPor() {
        return enviadoPor;
    }

    /**
     * Sets the value of the enviadoPor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnviadoPor(String value) {
        this.enviadoPor = value;
    }

}
