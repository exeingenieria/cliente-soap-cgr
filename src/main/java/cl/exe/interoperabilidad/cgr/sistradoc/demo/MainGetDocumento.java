package cl.exe.interoperabilidad.cgr.sistradoc.demo;

import cl.exe.interoperabilidad.cgr.sistradoc.GetDocumentoRequest;
import cl.exe.interoperabilidad.cgr.sistradoc.GetDocumentoResponse;
import cl.exe.interoperabilidad.cgr.sistradoc.TomadorDeRazon;

/**
 * Hello world!
 *
 */
public class MainGetDocumento 
{
    public static void main( String[] args )
    {
    	try {
	    	TomadorDeRazon tomador = new TomadorDeRazon();
	    	
	    	GetDocumentoRequest getDocReq = new GetDocumentoRequest();
	    	getDocReq.setAnno("2018");
	    	getDocReq.setCodRegion("13");
	    	getDocReq.setFolio("349");
	    	getDocReq.setServicio("352");
	    	getDocReq.setTipoDocCod("02");
	    	GetDocumentoResponse getDocRes = tomador.getDocumento(getDocReq);
	    	
	    	if (getDocRes.getGetDocumentoReturn() != null) {
	    		System.out.println("Exito: " + getDocRes.getGetDocumentoReturn());
	    		
	    		if (getDocRes.getGetOficioReturn() != null) {
	    			System.out.println("Exito: " + getDocRes.getGetOficioReturn());
	    		}
	    	} else {
	    		System.out.println("Error: " + getDocRes.getError());
	    	}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
