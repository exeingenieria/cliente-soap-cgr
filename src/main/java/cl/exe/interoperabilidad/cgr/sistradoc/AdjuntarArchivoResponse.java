
package cl.exe.interoperabilidad.cgr.sistradoc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="adjuntarArchivoReturn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "adjuntarArchivoReturn"
})
@XmlRootElement(name = "adjuntarArchivoResponse")
public class AdjuntarArchivoResponse {

    @XmlElement(required = true)
    protected String adjuntarArchivoReturn;

    /**
     * Gets the value of the adjuntarArchivoReturn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdjuntarArchivoReturn() {
        return adjuntarArchivoReturn;
    }

    /**
     * Sets the value of the adjuntarArchivoReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdjuntarArchivoReturn(String value) {
        this.adjuntarArchivoReturn = value;
    }

}
