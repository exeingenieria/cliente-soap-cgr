package cl.exe.interoperabilidad.cgr.sistradoc;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;

import org.apache.commons.configuration.PropertiesConfiguration;

public class TomadorDeRazon {
	private CgrServicioSistradocExtService service;
	private CgrServicioSistradocExt cgrWS;
	private String wsdlUrl;
	private String wsdlNamespace;
	private String wsdlLocalpart;
	
	public TomadorDeRazon() {
		try {
			PropertiesConfiguration config = new PropertiesConfiguration("soap-cgr.properties");
			
			this.wsdlUrl = (String) config.getProperty("url");
			this.wsdlNamespace = (String) config.getProperty("namespace");
			this.wsdlLocalpart = (String) config.getProperty("localpart");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public AdjuntarArchivoResponse adjuntarArchivo(AdjuntarArchivoRequest req) throws Exception {
		this.ping();
			
		AdjuntarArchivoResponse result = null;
		
		try {
			this.conectar();
			result = cgrWS.adjuntarArchivo(req);
		} catch(Exception e) {
			e.printStackTrace();
			throw new Exception("Error comunicacion al servicio CGR");
		}
		
		return result;
	}
	
	public CrearDocumentoResponse crearDocumento(CrearDocumentoRequest documento) throws Exception {
		this.ping();
			
		CrearDocumentoResponse result = null;
		
		try {
			this.conectar();
			result = cgrWS.crearDocumento(documento);
		} catch(Exception e) {
			e.printStackTrace();
			throw new Exception("Error comunicacion al servicio CGR ");
		}
		
		return result;
	}
	
	public GetDocumentoResponse getDocumento(GetDocumentoRequest req) throws Exception {
		this.ping();
			
		GetDocumentoResponse result = null;
		
		try {
			this.conectar();
			result = cgrWS.getDocumento(req);
		} catch(Exception e) {
			e.printStackTrace();
			throw new Exception("Error comunicacion al servicio CGR ");
		}
		
		return result;
	}
	
	public GetEstadoDocResponse getEstadoDoc(GetEstadoDocRequest req) throws Exception {
		this.ping();
			
		GetEstadoDocResponse result = null;
		
		try {
			this.conectar();
			result = cgrWS.getEstadoDoc(req);
		} catch(Exception e) {
			e.printStackTrace();
			throw new Exception("Error comunicacion al servicio CGR ");
		}
		
		return result;
	}
	
	private void createWsService() throws MalformedURLException {
		if (service == null) {
 			service = new CgrServicioSistradocExtService (
	 					new URL(this.getWsdlUrl()) , 
	 					new QName(this.getWsdlNamespace(), this.getWsdlLocalpart()));				
		}
	}
	
	private void conectar () throws MalformedURLException {
		createWsService();
		cgrWS = service.getCgrServicioSistradocExtSoap11();
	}
	
	private void ping() throws Exception {
		URL url = new URL(this.getWsdlUrl());
		
		if (!validarConeccion(url)) {
			throw new Exception("Error comunicacion al servicio " + url.toString());
		}
	}
	
	private boolean validarConeccion(URL url){
		
		if (url == null) {
			return false;
		}
		
		String urlStr = url.toString();
		
		try {
			HttpURLConnection.setFollowRedirects(false);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");

			con.setConnectTimeout(2000);

			return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		} catch (java.net.SocketTimeoutException e) {
			System.out.println("ERROR: No se ha podido conectar al servicio " + urlStr + ": " + e);
			return false;
		} catch (java.io.IOException e) {
			System.out.println("ERROR: No se ha podido conectar al servicio " + urlStr + ": " 	+ e);
			return false;
		}
	}

	public String getWsdlUrl() {
		return wsdlUrl;
	}

	public void setWsdlUrl(String wsdlUrl) {
		this.wsdlUrl = wsdlUrl;
	}

	public String getWsdlNamespace() {
		return wsdlNamespace;
	}

	public void setWsdlNamespace(String wsdlNamespace) {
		this.wsdlNamespace = wsdlNamespace;
	}

	public String getWsdlLocalpart() {
		return wsdlLocalpart;
	}

	public void setWsdlLocalpart(String wsdlLocalpart) {
		this.wsdlLocalpart = wsdlLocalpart;
	}
}
