
package cl.exe.interoperabilidad.cgr.sistradoc;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.exe.interoperabilidad.cgr.sistradoc package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.exe.interoperabilidad.cgr.sistradoc
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AdjuntarArchivoRequest }
     * 
     */
    public AdjuntarArchivoRequest createAdjuntarArchivoRequest() {
        return new AdjuntarArchivoRequest();
    }

    /**
     * Create an instance of {@link GetDocumentoRequest }
     * 
     */
    public GetDocumentoRequest createGetDocumentoRequest() {
        return new GetDocumentoRequest();
    }

    /**
     * Create an instance of {@link GetEstadoDocRequest }
     * 
     */
    public GetEstadoDocRequest createGetEstadoDocRequest() {
        return new GetEstadoDocRequest();
    }

    /**
     * Create an instance of {@link CrearDocumento }
     * 
     */
    public CrearDocumento createCrearDocumento() {
        return new CrearDocumento();
    }

    /**
     * Create an instance of {@link GetDocumentoResponse }
     * 
     */
    public GetDocumentoResponse createGetDocumentoResponse() {
        return new GetDocumentoResponse();
    }

    /**
     * Create an instance of {@link GetEstadoDocResponse }
     * 
     */
    public GetEstadoDocResponse createGetEstadoDocResponse() {
        return new GetEstadoDocResponse();
    }

    /**
     * Create an instance of {@link AdjuntarArchivo }
     * 
     */
    public AdjuntarArchivo createAdjuntarArchivo() {
        return new AdjuntarArchivo();
    }

    /**
     * Create an instance of {@link CrearDocumentoRequest }
     * 
     */
    public CrearDocumentoRequest createCrearDocumentoRequest() {
        return new CrearDocumentoRequest();
    }

    /**
     * Create an instance of {@link CrearDocumentoResponse }
     * 
     */
    public CrearDocumentoResponse createCrearDocumentoResponse() {
        return new CrearDocumentoResponse();
    }

    /**
     * Create an instance of {@link GetDocumento }
     * 
     */
    public GetDocumento createGetDocumento() {
        return new GetDocumento();
    }

    /**
     * Create an instance of {@link GetEstadoDoc }
     * 
     */
    public GetEstadoDoc createGetEstadoDoc() {
        return new GetEstadoDoc();
    }

    /**
     * Create an instance of {@link AdjuntarArchivoResponse }
     * 
     */
    public AdjuntarArchivoResponse createAdjuntarArchivoResponse() {
        return new AdjuntarArchivoResponse();
    }

}
