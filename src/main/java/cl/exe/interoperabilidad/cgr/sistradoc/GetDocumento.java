
package cl.exe.interoperabilidad.cgr.sistradoc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="servicio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="preFijo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="folio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="postFijo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="anno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codRegion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoDocCod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "servicio",
    "preFijo",
    "folio",
    "postFijo",
    "anno",
    "codRegion",
    "tipoDocCod"
})
@XmlRootElement(name = "getDocumento")
public class GetDocumento {

    @XmlElement(required = true)
    protected String servicio;
    @XmlElement(required = true)
    protected String preFijo;
    @XmlElement(required = true)
    protected String folio;
    @XmlElement(required = true)
    protected String postFijo;
    @XmlElement(required = true)
    protected String anno;
    @XmlElement(required = true)
    protected String codRegion;
    @XmlElement(required = true)
    protected String tipoDocCod;

    /**
     * Gets the value of the servicio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicio() {
        return servicio;
    }

    /**
     * Sets the value of the servicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicio(String value) {
        this.servicio = value;
    }

    /**
     * Gets the value of the preFijo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreFijo() {
        return preFijo;
    }

    /**
     * Sets the value of the preFijo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreFijo(String value) {
        this.preFijo = value;
    }

    /**
     * Gets the value of the folio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolio() {
        return folio;
    }

    /**
     * Sets the value of the folio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolio(String value) {
        this.folio = value;
    }

    /**
     * Gets the value of the postFijo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostFijo() {
        return postFijo;
    }

    /**
     * Sets the value of the postFijo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostFijo(String value) {
        this.postFijo = value;
    }

    /**
     * Gets the value of the anno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnno() {
        return anno;
    }

    /**
     * Sets the value of the anno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnno(String value) {
        this.anno = value;
    }

    /**
     * Gets the value of the codRegion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodRegion() {
        return codRegion;
    }

    /**
     * Sets the value of the codRegion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodRegion(String value) {
        this.codRegion = value;
    }

    /**
     * Gets the value of the tipoDocCod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocCod() {
        return tipoDocCod;
    }

    /**
     * Sets the value of the tipoDocCod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocCod(String value) {
        this.tipoDocCod = value;
    }

}
