
package cl.exe.interoperabilidad.cgr.sistradoc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="crearDocumentoReturn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "crearDocumentoReturn"
})
@XmlRootElement(name = "crearDocumentoResponse")
public class CrearDocumentoResponse {

    @XmlElement(required = true)
    protected String crearDocumentoReturn;

    /**
     * Gets the value of the crearDocumentoReturn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrearDocumentoReturn() {
        return crearDocumentoReturn;
    }

    /**
     * Sets the value of the crearDocumentoReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrearDocumentoReturn(String value) {
        this.crearDocumentoReturn = value;
    }

}
